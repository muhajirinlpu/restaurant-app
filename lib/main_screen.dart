import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:learn_02/detail_screen_restaurant.dart';
import 'package:learn_02/model/restaurant_model.dart';
import 'package:dio/dio.dart';
import 'package:learn_02/widget/text_field.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: null,
      body: RestaurantList(),
    );
  }
}

class RestaurantList extends StatefulWidget {
  const RestaurantList({Key? key}) : super(key: key);

  @override
  State<RestaurantList> createState() => _RestaurantListState();
}

class _RestaurantListState extends State<RestaurantList> {
  _fetchData() async {
    Dio dio = Dio();
    Response response = await dio.get(
        "https://gist.githubusercontent.com/LittleFireflies/e8c08f316217b5018b76b3e5463da34d/raw/bf145725d1d9af2635b71bd6d5d9dc0b79712157/local_restaurant.json");

    return response;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: FutureBuilder(
        future: _fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                return const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(child: Text("Error receiving data.")),
                );
              }

              Response response = snapshot.data;

              Map<String, dynamic> payload = jsonDecode(response.data);

              List<RestaurantModel> restaurants =
                  (payload['restaurants'] as List<dynamic>)
                      .map((e) => RestaurantModel.fromJson(e))
                      .toList();
              
              return RestaurantsListViewBuilder(restaurants: restaurants);
          }
        },
      ),
    );
  }
}

class RestaurantsListViewBuilder extends StatefulWidget {
  const RestaurantsListViewBuilder({Key? key, required this.restaurants})
      : super(key: key);

  final List<RestaurantModel> restaurants;

  @override
  State<RestaurantsListViewBuilder> createState() =>
      _RestaurantsListViewBuilderState();
}

class _RestaurantsListViewBuilderState
    extends State<RestaurantsListViewBuilder> {
  TextEditingController? searchInputController;

  String searchInput = "";

  @override
  void initState() {
    searchInputController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    searchInputController!.dispose();

    super.dispose();
  }

  handleSearchInput(String val) {
    setState(() {
      searchInput = val;
    });
  }

  List<RestaurantModel> _getFilteredRestaurant() {
    return widget.restaurants.where((RestaurantModel restaurant) {
      if (searchInput.isEmpty) {
        return true;
      }

      return restaurant.name.contains(searchInput) ||
          restaurant.description.contains(searchInput) ||
          restaurant.city.contains(searchInput);
    }).toList();
  }

  Widget _getHeader() {
    return Column(
      children: [
        const Text(
          "Restaurant",
          textAlign: TextAlign.left,
          style: TextStyle(fontSize: 30.0),
        ),
        const Text(
          "Recommendation restaurant for you!",
          textAlign: TextAlign.left,
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: MyTextField(
            "Search",
            controller: searchInputController,
            onChange: handleSearchInput,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var restaurants = _getFilteredRestaurant();

    if (restaurants.isEmpty) {
      return _getHeader();
    }

    return ListView.builder(
      itemCount: restaurants.length,
      itemBuilder: (context, index) {
        RestaurantModel restaurant = restaurants[index];

        var card = InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return DetailScreenRestaurant(restaurant: restaurant);
            }));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Hero(
                        tag: restaurant.id,
                        child: Image.network(restaurant.pictureUrl)),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            restaurant.name,
                            style: const TextStyle(fontSize: 16.0),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              const Icon(Icons.star),
                              Text(restaurant.rating.toString()),
                            ],
                          ),
                          Row(
                            children: [
                              const Icon(Icons.pin_drop),
                              Text(restaurant.city),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );

        if (index == 0) {
          return Column(
            children: [
              _getHeader(),
              card,
            ],
          );
        }

        return card;
      },
    );
  }
}
