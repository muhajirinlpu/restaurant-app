import 'package:flutter/material.dart';
import 'package:learn_02/splash.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          title: 'Restaurant',
          theme: ThemeData(),
          home: const SplashScreen(),
        );
      },
    );
  }
}
